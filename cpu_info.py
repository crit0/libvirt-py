#!/usr/bin/python

from __future__ import print_function
import sys
import libvirt


conn = libvirt.open('qemu:///system')
if conn == None:
	print('Failed to open connection to qemu:///system', \
			file=sys.stderr)
	exit(1)

vcpus = conn.getMaxVcpus(None) # type as input
print('Maximum support virtual CPUs: ' + str(vcpus))

# Host overview
nodeinfo = conn.getInfo()
print('Number of CPUs: ' + str(nodeinfo[2]))
print('MHz of CPUs: ' + str(nodeinfo[3]))
print('Number of NUMA nodes: ' + str(nodeinfo[4]))
print('Number of CPU sockets: ' + str(nodeinfo[5]))
print('Number of CPU cores per socket: ' + str(nodeinfo[6]))
print('Number of CPU threads per core: ' + str(nodeinfo[7]))

map = conn.getCPUMap()
print('CPUs: ' + str(map[0]))
print('Available: ' + str(map[1]))

# Stats
stats = conn.getCPUStats(0)

print('kernel: ' + str(stats['kernel']))
print('idle: ' + str(stats['idle']))
print('user: ' + str(stats['user']))
print('iowait: ' + str(stats['iowait']))


conn.close()
exit(0)
