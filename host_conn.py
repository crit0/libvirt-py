#!/usr/bin/python

from __future__ import print_function
import sys
import libvirt

conn = libvirt.open('qemu:///system')
if conn == None:
	print('Failed to open connection to qemu:///system', \
			file=sys.stderr)
	exit(1)

# URI
uri = conn.getURI()
print('Canonical URI: ' + uri)
print('Connectionis = ' + str(conn.isAlive()))

# is connection encrypted(1) or not(0)
if conn.isEncrypted() == 1:
	print('Connection is Encrypted')
else:
	print('Connection is Not Encrypted')

# is connection secure (either encrypted or running on a private channel(unix socket))
if conn.isSecure() == 1:
	print('Connection is Secure')
else:
	print('Connection is Not Secure')


