#!/usr/bin/python

from __future__ import print_function
import sys
import libvirt

# Open a connection
conn = libvirt.open('qemu:///system')
if conn == None:
	print('Failed to open connection to qemu:///system', \
			file=sys.stderr)
	exit(1)
print('Connection successful.')

conn.close()
print('Connection closed.')

# Read Only connection
conn = libvirt.openReadOnly('qemu:///system')
if conn == None:
	print('Failed to open connection to qemu:///system', \
			file=sys.stderr)
	exit(1)
print('ReadOnly Connection successful.')

conn.close()
print('ReadOnly Connection closed.')

exit(0)
