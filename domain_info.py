#!/usr/bin/python

from __future__ import print_function
import sys
import libvirt
from xml.dom import minidom

if len(sys.argv) > 2:
	print("Too many arguments!", file=sys.stderr)
	exit(1)
elif len(sys.argv) < 2:
	print("Too few arguments!", file=sys.stderr)
	exit(1)

conn = libvirt.open('qemu:///system')
if conn == None:
	print('Failed to open connection to qemu:///system', \
			file=sys.stderr)
	exit(1)

dom_name = sys.argv[1]

dom = conn.lookupByName(dom_name)

id = dom.ID()
if id == -1:
	print('The domain is not running so has no ID.')
else:
	print('The ID of the domain is ' + str(id))

type = dom.OSType()
print('The OS type of the Domain is "' + type + '"')

snap = dom.hasCurrentSnapshot()
if snap == 0:
	print('There are currently no snapshots')
else:
	print('The snapshot flag is currently set to ' + str(snap))

# only available on certain hypervisors/qemu-guest-agent
#name = dom.hostname()
#print('The hostname of the domain is ' + str(name))

# hardware information
state, maxmem, mem, cpus, cput = dom.info()
# state can be in several states
if state == 0:
	print('The state is "Undefined"')
elif state == 1:
	print('The state is "Running"')
elif state == 2:
	print('The state is "Blocked"')
elif state == 3:
	print('The state is "Paused"')
elif state == 4:
	print('The state is "Shutdown"')
elif state == 5:
	print('The state is "Shutoff"')
elif state == 6:
	print('The state is "Crashed"')
elif state == 7:
	print('The state is "PM Suspended"')
else:
	print('The domain is in an unknown state')

print('The max memory is ' + str(maxmem/1024) + 'MB')
print('The max memory is ' + str(dom.maxMemory()/1024) + 'MB')
print('The memory is set at ' + str(mem/1024) + 'MB')
print('The number of cpus is ' + str(cpus))
if dom.isActive == True:
	print('The number of max cpus is ' + str(dom.maxVcpus()))
print('The cpu time is ' + str(cput))

# check if Active
flag = dom.isActive()
if flag == True:
	print('The domain is active.')
else:
	print('The domain is inactive.')

# check interface information
if flag == True:
	ifaces = dom.interfaceAddresses(libvirt.VIR_DOMAIN_INTERFACE_ADDRESSES_SRC_ARP, 0)
	ip_addr = ifaces['vnet0']['addrs'][0]['addr']
	print('The VM has an ip of ', str(ip_addr))
	#for key, value in ifaces.items():
	#	print(key, " :: ", value)

conn.close()
exit(0)
