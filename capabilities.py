#!/usr/bin/python

from __future__ import print_function
import sys
import libvirt

# Get information on the virtualization host
conn = libvirt.openReadOnly('qemu:///system')
if conn == None:
	print('Failed to open connection to qemu:///system', \
			file=sys.stderr)
	exit(1)

caps = conn.getCapabilities() # will return xml string
print('Capabilities:\n' + caps)

conn.close()

exit(0)
