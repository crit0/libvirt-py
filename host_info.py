#!/usr/bin/python

from __future__ import print_function
import sys
import libvirt


conn = libvirt.open('qemu:///system')
if conn == None:
	print('Failed to open connection to qemu:///system', \
			file=sys.stderr)
	exit(1)

host = conn.getHostname()
print('Hostname: ' + host)

vcpus = conn.getMaxVcpus(None) # type as input
print('Maximum support virtual CPUs: ' + str(vcpus))

# Host overview
nodeinfo = conn.getInfo()
print('Model: ' + str(nodeinfo[0]))
print('Memory size: ' + str(nodeinfo[1]) + 'MB')
print('Number of CPUs: ' + str(nodeinfo[2]))
print('MHz of CPUs: ' + str(nodeinfo[3]))
print('Number of NUMA nodes: ' + str(nodeinfo[4]))
print('Number of CPU sockets: ' + str(nodeinfo[5]))
print('Number of CPU cores per socket: ' + str(nodeinfo[6]))
print('Number of CPU threads per core: ' + str(nodeinfo[7]))

# Free Memory
numnodes = nodeinfo[4]
memlist = conn.getCellsFreeMemory(0, numnodes)

cell = 0
for cellfreemem in memlist:
	print('Node ' + str(cell) + ': ' + str(cellfreemem) + ' bytes free memory')
	cell += 1

# Free Pages
pages = [2048]
start = 0
cellcount = 4
buf = conn.getFreePages(pages, start, cellcount)

i = 0
for page in buf:
	print("Page Size: " + str(pages[i]) + \
			" Available pages: " + str(page))
	++i

# Virt type
print('Virtualization type: ' + conn.getType())

# Security Model (is SELinux used)
model = conn.getSecurityModel()
print('Security Model: ' + model[0] + " " + model[1])

conn.close()
exit(0)
