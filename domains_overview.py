#!/usr/bin/python

from __future__ import print_function
import sys
import libvirt


conn = libvirt.open('qemu:///system')
if conn == None:
	print('Failed to open connection to qemu:///system', \
			file=sys.stderr)
	exit(1)

domain_id = 3
dom = conn.lookupByID(domain_id)
if dom == None:
	print('Failed to get the domain object', file=sys.stderr)

domain_name = 'testvm1'
dom = conn.lookupByName(domain_name)
if dom == None:
	print('Failed to get the domain object', file=sys.stderr)

# Listing Active Domains
domains_ids = conn.listDomainsID()
if domains_ids == None:
	print('Failed to get a list of domain IDs', \
			file=sys.stderr)

print("Active domains IDs:")
if len(domains_ids) == 0:
	print('  None')
else:
	for domain_id in domains_ids:
		print(' ' + str(domain_id))


# Listing All Domains
domain_names = conn.listAllDomains()
if conn == None:
	print('Failed to get a list of domain names', file=sys.stderr)

print("----- All Domains -----")
for domain_name in domain_names:
	print(domain_name.name())


# Listing Inactive Domains
domain_names = conn.listDefinedDomains()
if conn == None:
	print('Failed to get a list of domain names', file=sys.stderr)

#domain_ids = conn.listDomainsID()
#if domain_ids == None:
#	print('Failed to get a list of domain IDs', \
#			file=sys.stderr)
#if len(domain_ids) != 0:
#	for domainID in domain_ids:
#		domain = conn.lookupByID(domain_id)
#		domain_names.append(domain.name)

print("----- All inactive domains -----") # active domains are returned as virDomain objects not strings
if len(domain_names) == 0:
	print(' None')
else:
	for domain_name in domain_names:
		if isinstance(domain_name, str):
			print(domain_name)
		else:
			continue

# Listing All Active Domains
domain_names = conn.listDefinedDomains()
if conn == None:
	print('Failed to get a list of domain names', file=sys.stderr)

print("----- All inactive domains -----") # active domains are returned as virDomain objects not strings
if len(domain_names) == 0:
	print(' None')
else:
	for domain_name in domain_names:
		if isinstance(domain_name, libvirt.virDomain):
			print(domain_name[0].name())
		else:
			continue

conn.close()
exit(0)
